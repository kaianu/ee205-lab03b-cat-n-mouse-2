#!/bin/bash

DEFAULT_MAX_NUMBER=2048

A_GUESS=

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo OK cat, I\'m thinking of a number from 1 to $THE_MAX_VALUE.  Make a guess:

read A_GUESS

while [ "$A_GUESS" -lt 1 ] || [ "$A_GUESS" -gt "$THE_MAX_VALUE" ]
do
   if [ "$A_GUESS" -lt 1 ] || [ "$A_GUESS" -gt "$THE_MAX_VALUE" ]
   then
      echo Incorrect value. The number should be in between 1 and 2048, try again.
   read A_GUESS
   fi
done

echo "$A_GUESS"

while [ "$A_GUESS" != "$THE_NUMBER_IM_THINKING_OF" ];
do
   if [ "$A_GUESS" -gt "$THE_NUMBER_IM_THINKING_OF" ]
   then
      echo No cat... the number I\'m thinking of is smaller than $A_GUESS
   fi

   if [ "$A_GUESS" -lt "$THE_NUMBER_IM_THINKING_OF" ]
   then 
      echo No cat... the number I\'m thinking of is larger than $A_GUESS
   fi 
   read A_GUESS
done 

echo You got me...
echo The number I was thinking of was $THE_NUMBER_IM_THINKING_OF
echo "      \    /\\"
echo "       )  ( ')"
echo "      (  /  )"
echo "       \\(__)|"
